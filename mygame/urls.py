from django.conf.urls import patterns, url
from mygame import views

urlpatterns = patterns('',
        url(r'^$', views.mainMenu, name = 'mainMenu'),
        url(r'^add_session/$', views.add_session, name = 'add_session'),
        url(r'^gameSession/(?P<gameSession_name_slug>[\w\-]+)/$', views.Battleground, name = 'BATTLE'),
        url(r'^register/$', views.register, name = 'register'),
        url(r'^login/$', views.user_login, name = 'login'),
        url(r'^logout/$', views.user_logout, name = 'logout'),
)