from django import forms
from django.contrib.auth.models import User
from mygame.models import  gameSession, Player

class gameSessionForm(forms.ModelForm):
    name = forms.CharField(max_length=128, help_text="Please enter the gameSession name.")
    slug = forms.CharField(widget = forms.HiddenInput(), required = False)
    class Meta:
        model = gameSession
        fields = ('name',)

class UserForm(forms.ModelForm):
    password = forms.CharField(widget = forms.PasswordInput())
    class Meta:
        model = User
        fields = ('username', 'email', 'password')

class PlayerForm(forms.ModelForm):
    numberOfWins = forms.IntegerField(widget = forms.HiddenInput(), initial = 0)
    numberOfLoses = forms.IntegerField(widget = forms.HiddenInput(), initial = 0)
    class Meta:
        model = Player
        fields = ('picture',)