from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User


class Player(models.Model):
    user = models.ForeignKey(User)
    numberOfWins = models.IntegerField(default = 0)
    numberOfLoses = models.IntegerField(default = 0)
    picture = models.ImageField(upload_to = 'profile_images', blank = True)

    def win(self):
    	self.numberOfWins += 1

    def lose(self):
    	self.numberOfLoses += 1

    def __str__(self):
        return self.user.username


class gameSession(models.Model):
    name = models.CharField(max_length = 128, unique = True)
    isFree = models.BooleanField(default = True)
    slug = models.SlugField()
    player1 = models.ForeignKey(User, null = True, related_name = "%(app_label)s_%(class)s_related")
    player2 = models.ForeignKey(User, null = True, related_name = '+')

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(gameSession, self).save(*args, **kwargs)

    def __str__(self):
    	return self.name

