from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from mygame.models import gameSession, Player
from mygame.forms import gameSessionForm, UserForm, PlayerForm

# Gives us a list of gameSessions
@login_required
def mainMenu(request):
    session_list = gameSession.objects.filter(isFree = True)
    context_dict = {'sessions': session_list}
    return render(request, 'mygame/mainMenu.html', context_dict)

@login_required
def Battleground(request, gameSession_name_slug):
    context_dict = {'player1': None, 'player2': None, 'this': None}
    try:
        currentSession = gameSession.objects.get(slug = gameSession_name_slug)
        print(currentSession.player1)
        print(currentSession.player2)
        if currentSession.player1 == None:
            currentSession.player1 = request.user
            currentSession.save()
        elif currentSession.player2 == None:
            currentSession.player2 = request.user
            currentSession.save()
        else:
            currentSession.isFree = False
            currentSession.save()
        context_dict['player2'] = currentSession.player2
        context_dict['player1'] = currentSession.player1
        context_dict['this'] = currentSession
    except gameSession.DoesNotExist:
        pass
    return render(request, 'mygame/session.html', context_dict)

@login_required
def add_session(request):
    if request.method == 'POST':
        form = gameSessionForm(request.POST)
        if form.is_valid():
            form.save(commit = True)
            return mainMenu(request)
        else:
            print(form.errors)
    else:
        form = gameSessionForm()
    return render(request, 'mygame/add_session.html', {'form': form})

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/mygame/')

def register(request):
    registered = False
    if request.method == 'POST':
        user_form = UserForm(data = request.POST)
        profile_form = UserProfileForm(data = request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            profile = profile_form.save(commit = False)
            profile.user = user
            if 'picture' in request.FILES:
                profile.picture = request.FILES['picture']
            profile.save()
            registered = True
        else:
            print (user_form.errors, profile_form.errors)
    else:
        user_form = UserForm()
        profile_form = UserProfileForm()

    return render(request,
            'mygame/register.html',
            {'user_form': user_form, 'profile_form': profile_form, 'registered': registered}
            )

def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username = username, password = password)
        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/mygame/')
            else:
                return HttpResponse("Your Rango account is disabled.")
        else:
            print ("Invalid login details: {0}, {1}".format(username, password))
            return HttpResponse("Invalid login details supplied.")
    else:
        return render(request, 'mygame/login.html', {})







