from django.contrib import admin
from mygame.models import gameSession, Player

class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug':('name',),}

admin.site.register(gameSession, CategoryAdmin)
admin.site.register(Player)